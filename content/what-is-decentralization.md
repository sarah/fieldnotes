+++
date = "2018-09-21T09:27:27-07:00"
title = "What is Decentralization?"
tags = ["decentralization"]
draft = false
+++

Decentralization is important because building systems that distribute power is
important. Building systems that resist abuses of power is important.

In it's simplest definition, decentralization is the degree to which an entity
within the system can resist coercion and still function as part of the system.
Now be careful, coercion doesn't mean force, it means negative incentives to
align with an authority.

Note how nothing in the above definition explicitly references the distribution
or the ownership of entities.

Nodes can be distributed across whole planet and can be operated by dozens of
independent organizations and the system, as a whole, could still be centralized.

A great example of the above is Email. Email clients are very diverse, the same
with providers, however when it comes to decisions that impact the whole
system, a handful of centralized actors (and in particular, Google) have a very
large say in the outcome.

And here we come the concept of Practical Centralization. The idea that even if
a protocol is designed with autonomy in mind, the way that it has been
deployed/implemented has resulted in uneven power distribution. See: email
(gmail), ntp (google, canonical) etc.

Practical centralization is a hard concept to grasp because it isn't obvious
from reading a paper or protocol spec how adoption will play out. However, it is
nevertheless something that has to be designed against.

Hard coded defaults/trust anchors, centralized upgrade severs, user preference,
proprietary features, spam, ddos prevention...so many things can practically
cause [emergent centralization](https://fieldnotes.resistant.tech/defensive-decentralization/) and give a handful of people uneven control over the
whole system (and it's future direction).

We need to move beyond naive conceptualizations of decentralization (like the
percentage of nodes owned by an entity), and instead, holistically, understand how trust
and power are given, distributed and interact.

As an example let's take my favorite decentralized protocol: Ricochet. Assuming
the continued operation of the Tor network - ricochet users can continue
communicating with other uses without any interference or assistance from the
developers or anyone else.

If the project shut down tomorrow, or updated the software to attempt to censor specific clients, those ricochet peers would still work.

I also built on top of the ricochet protocol completely separate from the
canonical spec. My code can still interoperate should I choose that. It can
evolve. Ricochet is therefore decentralized at the Protocol, the
Political, and the Practical.

I kind of glanced over a big assumption there ("the continued operation of the
Tor network") - many would see this as evidence that Ricochet is not
a decentralized system because it relies on Tor, whose relay consensus is highly
centralized around the distribution of the directory server list from the Tor
network.

Does that make Ricochet centralized? I don't believe so.

I could argue that even with the existence of the directory server list that Tor
is more decentralized than not. Anyone can spin up a relay. The existence of the
centralized directory server list doesn't compromise the trust associated with
Tor's main function (routing and anonymizing traffic).

Even without that argument, I think it is worth pointing out that it is possible
to build decentralized systems on top of centralized ones. In fact, right now
it's the only option for many types of applications - IP addresses assignment is
not a decentralized process, internet routing is a highly centralized operation,
and going much further down the stack - we only have one planet.

The important principle here is that to be considered decentralized, a system
should not undermine the decentralization of the layers below it - and should work
to distribute trust & power equitably to the layers above it.

Decentralization is ultimately about distributing trust and power.

