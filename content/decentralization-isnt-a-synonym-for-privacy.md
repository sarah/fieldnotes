+++
date = "2018-07-13T11:19:24-07:00"
title = "Decentralization isn't a Synonym for Privacy"
+++

Decentralization makes certain kinds of attacks on privacy more difficult.

The truth is that it's a spectrum of attacks and adversaries. On the centralized cash side, you are vulnerable to single-point interception, and targeted censorship. 

On the decentralized side to you are vulnerable to small anonymity sets and, in many cases still, large scale correlation attacks. (And in many, many cases still [a complete lack of privacy at all](https://fieldnotes.resistant.tech/privacy-and-p2p-protocols/))

If your adversary is a state, you might be in trouble either way (depending on your threat model), but many of your centralized options are vulnerable to jurisdictional pressure. 

If your adversary is not-a-state-but-has-computing-resources-and-knows-statistics then, perhaps unintuitive, decentralization might not be the the best way to maintain privacy, because of the small anonymity sets (assuming the system you are using has some kind of privacy model).

For a concrete example, let's look to the case that inspired this thread: Money. 

Feds and banks are really good at tracing cash once there is a lead. The number of hops a bill makes from taken out of a bank to being put back in is pretty small.

Cash really isn't as anonymous as many people think it is - if your adversary is a state.

However, if your adversary isn't a state, cash is pretty damn anonymous despite the inherent centralization within the system.

On the flip side, some cryptocurrencies provide nice privacy models, aided by a layer of decentralization, but the small number of people transacting, and the public nature of the ledger explicitly widen the adversary set, and correlation attacks over small anonymity sets can be pretty bad and also pretty accessible.

Ultimately, we need to think of decentralization as a tool for distributing power, that just so happens to also improve particular kinds of privacy. 
