+++
date = "2018-10-09T10:51:11-07:00"
title = "Untrust as an alternative to Decentralize"
tags = ["untrusted","decentralized"]
+++

If the safety/operations/security of a system do not rely on trust, does centralization within the system matter?

This is a question I've been considering lately as I start to think through the consequences of Cwtch server deployments.

There are ways of using Cwtch that promote heavily centralized topologies e.g. only using a small number of Cwtch servers. The servers themselves though are untrusted - we assume they will try to do bad things and design the protocol around subverting or, at a minimum, detecting bad behavior.

Is that enough though? Can we really predicate consensual technology on untrust? Should we not seek to distribute any and all power to as many as possible?

It can be argued that decentralization follows from untrust. If I extend my untrust of a particular server through to the logical conclusion that I can't expect the server to always be available, then I should rely on multiple severs to achieve any goal I would use a particular server for.

Further, I should seek to use servers from different operator for similar reasons - not because I fear a single operator might be malicious (where we extend malice to cover failure-to-operate), but because I assume all operators are potentially malicious and wish to ensure my operation completes.

This argument only extends so far, up to the limitations of scale.

While distributing whole infrastructure between 2 or 3 parties is workable, distributing it between 1000 or 10,000 is practically unworkable. Somewhere between those extremes is a number of distinct parties, who untrust each other, which can be collectively called on to operate a globally accessible system without trust.

There is nothing to prevent such a group from being open, each party is incentivised (through untrust) to maintain connections with as many other parties as possible - this potentially leads to a scenario where it requires significant investment to become reliable (yet untrusted) infrastructure within the system (that doesn't seem like a terrible consequence though)

Given that, we are left with the task of designing a protocol such that all interactions (peer to peer & peer to infrastructure) are untrusted.

The more I think about it, the more I see [cwtch](https://cwtch.im) as living within this realm of protocols. Encouraging distribution of power/trust in the protocol layers on top of it, by providing a layer of untrusted (yet reliable) infrastructure on which to build.

Does this mean I'm giving up on decentralization? Definitely not, decentralization of power is a fundamental tenet. It just might be more useful to think about realizing that through untrust.

