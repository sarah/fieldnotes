+++
date = "2018-07-10T10:08:16-07:00"
title = "Federation is the Worst of all Worlds"
+++

The threat model and economics of federated systems devolve to concentrating trust in the hands of a few, while missing out on the scale advantages of purely centralized solutions.

Federation results in the data of users being subject to the whims of the owner of the federated instance. Administrators can see correspondence and derive social graphs trivially. They are also in a position to selectively censor inter-instance communication. All of this while gaining none of the benefits of scale and discovery that centralized systems provide.

All the privacy issues, none of the scale advantages.

Is that inherent to federated systems? I would argue yes, at least in the context of how we think about federation today. To achieve some level of usability, to present the user with a known interface we expect them to trust the federated server.

Without building consent and resistance into the protocol and infrastructure, we're just forcing most users to pick a new dictator for their data without any real basis for that choice.

Considering that one of the main goals of decentralized systems is privacy preservation, and thus, control distribution, we must develop better models than "the most popular federated instances gain full control over the users interactions".

I believe this requires building 2 layers of decentralized communal infrastructure. A privacy preserving persistence layer removed from any application (see also [cwtch](https://cwtch.im)). And an application layer which can interact with it, and provide features for it (microblogging, social networking, filesharing, collaborative editing etc.)

(first person to say blockchain loses.)

You need that first persistence layer to be communal and decentralized to prevent any entity being in a position do something like "all the DMs on this instance are readable by whoever admins it". Effectively you need to stop applications from reinventing base protocols, by providing an infrastructure layer that is privacy preserving.

(I've said this before, it should be trivial to e.g. build a dating app on top of something like signal, there is no reason developers should have to reinvent secure communication to build higher level applications)

There are a number of [open problems](https://openprivacy.ca/blog/2018/06/28/announcing-cwtch/) with this kind of approach. Reliability & Discoverability being the main two. However I don't think that either problem is intractable.

Going further I would say that even if the solutions to both problem involved introducing a layer of federated trust (e.g. nameservers), that trust would be more restricted, agile and controllable than could be achieved with traditionally federated systems.

In short, federation is the wrong model because it concentrates trusts, and we must look to building privacy-preserving peer-to-peer infrastructure if we have any hope of building a decentralized future.
