+++
date = "2018-07-24T10:20:42-07:00"
title = "Metadata Resistance and Multiple Devices"
draft = true
+++

Incorporating multiple devices into a metadata resistant protocols presents many distinct challenges.

First, you require strong consistency *between* devices, without that consistency you can end up with irreconcilable state differences between devices.

This need to sync devices in a strongly consistent fashion necessarily requires communication, and thus is subject to metadata analysis.

In Cwtch, we are considering a design proposal whereby this syncing is conducted over a Cwtch group. This solves many of the challenges present in syncing multiple devices in a metadata resistant fashion. Further we can actually afford to tighten our trust assumptions (because every device is assumed to be trusted, for the majority of the time at least).

That last assumption is an important one. In any multi-device design we must consider what happens when one of the devices gets compromised.

This is where alternative approaches to metadata resistant multi-device consistency struggle.

In a main device / secondary devices setup if your main device gets compromised there is very little you can do to recovery fully.
