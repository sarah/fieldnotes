+++
date = "2018-07-16T10:59:41-07:00"
title = "Propaganda Pheromones"
tags = ["speculation"]
+++

I have a long history of infatuation with the mechanisms at work in colonies of ants and other eusocial insects. The book that got me started down that path was Turtles, Termites and Traffic Jams by Mitchel Resnick which I read when I was 15 and which I think deserves it's own fieldnote. The idea that a system can be devoid of any leader but still fully functioning will forever be an inspiration for my work.

In that vein I was recently reading some papers on ["propaganda" pheromones](/dettorre2001.pdf) in [ant species](/regnier1971.pdf), and the idea of a protocol inspired by that concept has been gnawing at me.

Propaganda pheromones appear to be an emergent property of alarm pheromones which are released when an ant is attacked and that have evolved to alert other nestmates to the danger sot that they can flee or attack the invader.

Certain species of ants rely on stealing larvae from other ant species. They raid colonies and spray massive amounts of alarm pheromones as they do. The pheromones overwhelm the attacked colony workers & their flee response causes them to attack their own nestmates.

The attacking ants then steal the larvae and take it back to their colony where, once they are born, they perform worker functions for the nest - a function the attacking ants can't do! Amazing that these ant species evolved this elaborate system, but this aspect of the system is less interesting outside of the context, I think.

I've been thinking a lot about P2P networks recently, and the idea of competitive nodes that govern trust decisions based on local surrounding signals amuses me, even if I can't think of a practical application for it.

You would need a concept of "alarm" and "attack" (and thus "nest") that made sense.

One area where I think such a concept might work is in a shared-resource network e.g. a P2P filesystem.

A node low on a disk space might sent out alarm signals, causing peers to come to it's aid and adopting some of the files the node is responsible for, the peer would become the new owner.

If a peer receives too many alarms it causes them to break the connection from their neighbors, and not trust them.

How such a mechanism would impact load balancing and replication is I think an open question. It's a different take than the propagation and usage based distribution/balancing seen in most modern resource-sharing systems.

There are obviously a variety of attacks and balances to be made in such a system. To censor a server or file one might only need to overwhelm the server with requests to server a particular file - perhaps basing the alarming mechanism on the utilization of outgoing bandwidth might better?

Whether such a system is actually useful/practical/secure is highly speculative, but I think it is fun to think about.
