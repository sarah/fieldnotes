+++
date = "2018-07-14T09:21:36-07:00"
title = "On Emergent Centralization"
+++

There is a principle of Defensive Decentralization: when besieged, a well constructed decentralized system will further decentralize. 

The corollary of which is: A well constructed decentralized system will identify & attack emergent centralization.

A problem I've been considering recently is the tendency for decentralized systems to develop emergent centralization in response to non-adversarial conditions e.g. to improve scalability.

Some opportunistic centralization is great, even necessary for an effective system - but far too many protocols leave such behavior unchecked. 

[Federated systems/platforms suffer this issue](https://fieldnotes.resistant.tech/federation-is-the-worst-of-all-worlds/) so much I'd go as far to declare it systemic to federation general.

Email is a perfect example of an ostensibly decentralized, distributed system that, in defending itself from abuse and spam, became a highly centralized system revolving around a few major oligarchical organizations. The majority of email sent to today is likely to find itself being processed by the servers of one of these organizations.

It's worth stating that the centralization doesn't necessarily have to happen in-protocol like it does in email. Political centralization (over code or design) as seen in many blockchain projects is also a problem - though less relevant to this observation.

How we design systems to defend themselves from emergent centralization is, I think, an open problem.

Protocols such as [Ricochet](https://ricochet.im) and [Briar](https://briarproject.org) provide some of the answer, relying on strong peer-to-peer models that are difficult to directly exert any kind of control over. This kind of design comes with the downside of making certain desirable features very difficult.

[Cwtch](https://cwtch.im) is built on Ricochet with the aim of making many of those desirable features possible. However, one the risks of such designs is that emergent centralization becomes entrenched. As such Cwtch Servers can be seen as having built in defensive decentralization - the more peers/groups using a server, the greater the anonymity set. However, servers don't scale, so performance suffers as the number of connected peers increases, triggering the peers to move to another server.

Like a slime mold model, I can imagine peers in Cwtch transitioning from clustering around a few larger servers, to being spread out among many smaller servers, and back again.

There are a few open problems around designing such a mechanism: How do you tune peer behavior to optimize for both anonymity set and decreased latency? Can we tune server behavior to encourage defensively decentralization (e.g. by deliberately decreasing overall performance)?

It is something I hope to explore in further depth soon.



